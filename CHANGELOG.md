# Setup on 02/13/2024
- Initialized repository
- Created README.md
- Created CONTRIBUTING.md
- Created LICENSE
- Setup nix development shell

# v0.0.0 02/14/2024
- Updated shell.nix to work with GTK
- Setup GTK application initilization
- Basic blank GTK 3.0 window with title

# v0.0.1 02/14/2024
- Fixed typo in function name
- Added a (currently) useless child button to the app
- Set the main window to allow for children

# v0.0.2 02/14/2024
- Refactored code out of `main.rs`
- Created modules to handle the app and the UI independent of `main.rs`

# v0.0.3 02/15/2024
- Undid the refactor for now
- Created TODO.md
- Created base menubar
- Replaced button with simple text input

# v0.1.0 02/16/2024
- Replaced text entry with a text viewer
- Implemented propper text buffer
- Created content box for all content
- Set up basic text wrapping

# v0.1.1 02/17/2024
- Set release optimizations for size
- Set all content inside a scroll box
- Added client side window controls

# v0.1.2 02/17/2024
- Refactored code out of `main.rs`
- Created `user_interface.rs`, `menu_bar.rs` and, `text_box.rs` as modules
- Fixed version number error in Cargo.toml

# v0.1.3 02/18/2024
- Added a GTK icon for the app that lives in the binary
- Added the ability to detect change in the text buffer
- Added the ability to detect a word completion
- Added a handler for word changes

# v0.1.4 02/19/2024
- Restructured codebase to adopt a hierarchical struct-based architecture
- Introduced a new module, `structs.rs`, to define the core data structures
- Implemented separate implementation files for each struct to encapsulate their behaviors
- Centralized state management by allowing `main.rs` full control over top-level structs
- Refactored existing code to align with the new hierarchical struct-based architecture

# v0.2.0 02/19/2024
- Implemented `Debug`, `Clone`, and `PartialEq` on all structs
- Created very basic save and load system
- Moved `character_change_handler` to `undo_redo.rs`
- Cleaned up `main.rs`

# v0.2.1 02/19/2024
- Cleaned up `main.rs` for better readability

# v0.2.2 02/20/2024
- Added support for custom paths for saving and loading
- Added file picker window for saving and loading

# v0.3.0 02/22/2024
- Added support for undo and redo actions
- Moved GUI app fucntions out of main

# v0.4.0 02/23/2024
- Added a line counter
- Made line counter part of the window content
- Made line counter work with save load system
- Made line counter work with the undo redo system

# v0.4.1 02/23/2024
- Fixed TODO.md to refelct v0.4.0
- Changed icon to make it less sharp

# v0.4.2 02/23/2024
- Added detection of return key
- Fixed bug where line counter would not count new line till it was changed
- Automatic newline added when saving files if last line was not blank

# v0.4.3 02/25/2024
- Refactored `save_buffer_to_file` function to use Result for error handling.
- Refactored `load_buffer_from_file` function to use Result for error handling.
- Replaced inline code with helper function `create_file_dialog` for cleaner code.
- Refactord save_load_backend.rs to impove readablity
- Updated main.rs to reflect changes in saving and loading

# v0.4.4 02/26/2024
- Changed `character_changed_handler` to use `unwrap_or_defualt` when getting the GString
- Created `event_handler.rs` to move GTK event handling away from `main.rs`
- Removed use of `expect` and `unwrap` in `load` and `save` to prevent panics
- Moved `run_app` out of `main.rs`

# v0.4.5 02/27/2024
- Added `_init` to funcion names in `event_handler.rs` to better describe the functions by name

# v0.4.6 02/28/2024
- Changed `expect` to `init` in `main.rs` when initializing GTK
- Changed `user_interface.rs` to properly handle a failure to set the app icon

# v0.4.7 02/29/2024
- Fixed bug where the cancel buttone would not close the file picker
- Updated `load_file_into_buffer` to change the name of the window to reflect the oppened file
- Updated `user_interface.rs` to properly set the name of the app window

# v0.5.0 03/01/2024
- Fixed formatting in `structs.rs`
- Renamed `save_file_to_buffer` to `save_as_file_to_buffer`
- Created new `save_file_to_buffer` function to save active files
- Made save and load fucntions update the window name with the active file
- Added `active_file` to `window_content` to track the active file
- Made `user_interface.rs` set the title to New File if there is no file
- Save now will save automatically to the last opened file
- Save As will now act as Save used to and will open the GTK file picker 

# v0.5.1 03/02/2024
- Fixed bug in file picker wehre it would not show the right name
- Added new file functionality
- Fixed formatting in `user_interface.rs`

# v0.5.2 03/03/2024
- Fully implemented the File submenu in the header bar
- Moved functions for the File submenu into `file_menu_handler.rs`
- Cleaned up `event_handler.rs`

# v0.5.3 03/04/2024
- Made `event_handler.rs` more readable
- Added an acceleration group to program to hendle hotkeys
- Added hotkeys to all of the file menu options
- Moved the handler for quitting into it's own function for consistency

# v0.6.0 03/05/2024
- Cleaned up formatting in `*_menu_impl`
- Removed the "Delete Slection" item as doing in the GUI is more intuitive anyways
- Added hotkeys for all menu items
- Moved all syntax highlighting to icebox
- Moved Edit menu implementation to their own handler file
- All non functioning menu buttons will now log to console if unimplemented

# v0.6.1 03/06/2024
- Updated formating in `undo_redo.rs`
- Implemented the Cut, Copy, and Paste menu items

# v0.6.2 03/11/2024
- Updated `shell.nix` for better reproducibility and stability
- Cleaned up the changelog.
