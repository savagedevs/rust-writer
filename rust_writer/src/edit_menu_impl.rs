/*
edit_menu_impl.rs - Implementation of the EditMenuItems struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{Menu, MenuItem};
use crate::structs::EditMenuItems;

impl EditMenuItems {
    pub fn new() -> Self {
        let edit_submenu: MenuItem        = MenuItem::with_label("Edit");
        let edit_menu_holder: Menu        = Menu::new();
    
        let edit_undo: MenuItem           = MenuItem::with_label("Undo");
        let edit_redo: MenuItem           = MenuItem::with_label("Redo");
        let edit_cut: MenuItem            = MenuItem::with_label("Cut");
        let edit_copy: MenuItem           = MenuItem::with_label("Copy");
        let edit_paste: MenuItem          = MenuItem::with_label("Paste");
        let edit_preferences: MenuItem    = MenuItem::with_label("Preferences");
    
        edit_menu_holder.append(&edit_undo);
        edit_menu_holder.append(&edit_redo);
        edit_menu_holder.append(&edit_cut);
        edit_menu_holder.append(&edit_copy);
        edit_menu_holder.append(&edit_paste);
        edit_menu_holder.append(&edit_preferences);
    
        edit_submenu.set_submenu(Some(&edit_menu_holder));

        EditMenuItems {
            edit_submenu,
            edit_menu_holder,
            edit_undo,
            edit_redo,
            edit_cut,
            edit_copy,
            edit_paste,
            edit_preferences
        }
    }
}

impl Default for EditMenuItems {
    fn default() -> Self {
        Self::new()
    }
}
