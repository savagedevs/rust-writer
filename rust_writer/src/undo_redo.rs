/*
undo_redo.rs - Functions for undo and redo operations on a GTK TextBuffer.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::TextBuffer;
use crate::structs::UndoRedoHandler;

pub fn character_change_handler(text_buffer: &TextBuffer, undo_redo: &mut UndoRedoHandler) {
    println!("Text Changed");
    
    let start_iter: gtk::TextIter = text_buffer.start_iter();
    let end_iter: gtk::TextIter = text_buffer.end_iter();

    let raw_text: gtk::glib::GString = text_buffer.text(&start_iter, &end_iter, true).unwrap_or_default();
    let text_slice: String = raw_text.to_string();

    if text_slice.len() <= 2 {
        return;
    }
    let last_two_chars: &str = &text_slice[text_slice.len() - 2..];

    if !last_two_chars.starts_with(' ') && last_two_chars.chars().nth(1) == Some(' ') {
        undo_redo.push(text_slice);
        println!("Word detected");
    }
}

pub fn undo_handler(text_buffer: &TextBuffer, undo_redo: &mut UndoRedoHandler) {
    if let Some(text) = undo_redo.undo() {
        text_buffer.set_text(text.as_str());
    } else {
        println!("Nothing to undo.");
    }
}

pub fn redo_handler(text_buffer: &TextBuffer, undo_redo: &mut UndoRedoHandler) {
    if let Some(text) = undo_redo.redo() {
        text_buffer.set_text(text.as_str());
    } else {
        println!("Nothing to redo.");
    }
}
