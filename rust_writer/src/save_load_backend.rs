/*
save_load_backend.rs - Functions for saving and loading a file to and from a GTK TextBuffer.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use std::fs::{self, File};
use std::io::Write;
use std::error::Error;
use std::path::PathBuf;
use gtk::prelude::*;
use gtk::{FileChooserAction, FileChooserDialog, Window};

pub fn save_as_buffer_to_file(buffer: &gtk::TextBuffer, parent_window: &Window, save_path: &mut PathBuf) -> Result<(), Box<dyn Error>> {
    let mut text: String = buffer
        .text(&buffer.start_iter(), &buffer.end_iter(), true)
        .ok_or("Failed to get GString")?
        .to_string();

    if !text.ends_with('\n') {
        text.push('\n');
    }

    let file_dialog: FileChooserDialog = create_file_dialog("Save", parent_window, FileChooserAction::Save);
    if file_dialog.run() == gtk::ResponseType::Ok {

        let path:PathBuf = file_dialog.filename().ok_or("Could not get path")?;
        File::create(&path)?.write_all(text.as_bytes())?;

        let path_str: &str = path.to_str().unwrap_or("New File");
        parent_window.set_title(format!("Rust Writer: {}", path_str).as_str());

        *save_path = path;
    }

    file_dialog.close();
    Ok(())
}

pub fn save_buffer_to_file(buffer: &gtk::TextBuffer, parent_window: &Window, save_path: &mut PathBuf) -> Result<(), Box<dyn Error>> {
    if save_path.to_str().unwrap_or("New File") == "New File" {
        save_as_buffer_to_file(buffer, parent_window, save_path)?
    }

    let mut text: String = buffer
        .text(&buffer.start_iter(), &buffer.end_iter(), true)
        .ok_or("Failed to get GString")?
        .to_string();

    if !text.ends_with('\n') {
        text.push('\n');
    }

    File::create(&save_path)?.write_all(text.as_bytes())?;

    Ok(())
}

pub fn load_file_into_buffer(buffer: &gtk::TextBuffer, parent_window: &Window, load_path: &mut PathBuf) -> Result<(), Box<dyn Error>> {
    let file_dialog:FileChooserDialog = create_file_dialog("Open", parent_window, FileChooserAction::Open);
    if file_dialog.run() == gtk::ResponseType::Ok {
        let path = file_dialog.filename().ok_or("Could not get path")?;
        buffer.set_text(&fs::read_to_string(&path)?);

        let path_str: &str = path.to_str().unwrap_or("New File");
        parent_window.set_title(format!("Rust Writer: {}", path_str).as_str());

        *load_path = path;
    }

    file_dialog.close();
    Ok(())
}

fn create_file_dialog(name: &str, parent_window: &Window, action: FileChooserAction) -> FileChooserDialog {
    let title: String = name.to_string() + " File";

    let file_dialog:FileChooserDialog = FileChooserDialog::new(
        Some(title.as_str()),
        Some(parent_window),
        action,
    );
    file_dialog.add_button(name, gtk::ResponseType::Ok);
    file_dialog.add_button("Cancel", gtk::ResponseType::Cancel);

    file_dialog
}
