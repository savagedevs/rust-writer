/*
file_menu_impl.rs - Implementation of the FileMenuItems struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{Menu, MenuItem};
use crate::structs::FileMenuItems;

impl FileMenuItems {
    pub fn new() -> Self {
        let file_submenu: MenuItem    = MenuItem::with_label("File");
        let file_menu_holder: Menu    = Menu::new();

        let file_new: MenuItem        = MenuItem::with_label("New");
        let file_open: MenuItem       = MenuItem::with_label("Open");
        let file_save: MenuItem       = MenuItem::with_label("Save");
        let file_save_as: MenuItem    = MenuItem::with_label("Save As...");
        let file_quit: MenuItem       = MenuItem::with_label("Quit");

        file_menu_holder.append(&file_new);
        file_menu_holder.append(&file_open);
        file_menu_holder.append(&file_save);
        file_menu_holder.append(&file_save_as);
        file_menu_holder.append(&file_quit);

        file_submenu.set_submenu(Some(&file_menu_holder));

        FileMenuItems {
            file_submenu,
            file_menu_holder,
            file_new,
            file_open,
            file_save,
            file_save_as,
            file_quit,
        }
    }
}

impl Default for FileMenuItems {
    fn default() -> Self {
        Self::new()
    }
}
