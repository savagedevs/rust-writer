/*
event_handler.rs - Handle GTK events for the app.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{Application, TextBuffer, AccelGroup};
use std::sync::{Arc, Mutex};
use crate::structs::{EditMenuItems, FileMenuItems, HeaderBarItems, SearchMenuItems, UndoRedoHandler, WindowContentItems};
use crate::user_interface::build_ui;
use crate::file_menu_handler::file_menu_handler;
use crate::edit_menu_hendler::edit_menu_handler;
use crate::search_menu_handler::search_menu_handler;

fn key_press_init(window_content_items: &WindowContentItems) {
    window_content_items.active_text_items.text_viewer.connect_key_press_event(move |_, event| {
        if event.keyval() == gtk::gdk::keys::constants::Return {
            println!("Return key pressed!");
            // TODO: Push an undo redo state on newlines
        }

        gtk::glib::Propagation::Proceed
    });
}

pub fn run_app(app: &Application) {
    let header_bar_items: HeaderBarItems = HeaderBarItems::new();
    let window_content_items: WindowContentItems = WindowContentItems::new();
    let accel_group: AccelGroup = AccelGroup::new();
    build_ui(app, &window_content_items, &header_bar_items, &accel_group);

    let text_buffer: &TextBuffer = &window_content_items.active_text_items.text_buffer;
    let line_counter_buffer: &TextBuffer = &window_content_items.line_counter_items.counter_buffer;

    let undo_redo_handler: Arc<Mutex<UndoRedoHandler>> = Arc::new(Mutex::new(UndoRedoHandler::new()));
    let edit_menu: EditMenuItems = header_bar_items.edit_menu_items;
    edit_menu_handler(text_buffer, line_counter_buffer, &edit_menu, &accel_group, &undo_redo_handler);

    let file_menu: FileMenuItems = header_bar_items.file_menu_items;
    file_menu_handler(file_menu, app, text_buffer, &accel_group);

    let search_menu: SearchMenuItems = header_bar_items.search_menu_items;
    search_menu_handler(&search_menu, &accel_group);

    key_press_init(&window_content_items);
}
