/*
active_text_impl.rs - Implementation of the LineCounterItems struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{TextBuffer, TextTagTable, TextView, WrapMode};
use crate::structs::LineCounterItems;

impl LineCounterItems {
    pub fn new() -> Self {
        let counter_tag_table = TextTagTable::new();
        let counter_buffer = TextBuffer::new(Some(&counter_tag_table));
        let counter_viewer = TextView::with_buffer(&counter_buffer);
    
        counter_viewer.set_wrap_mode(WrapMode::None);
        counter_viewer.set_vexpand(true);
        counter_viewer.set_editable(false);
        counter_viewer.set_cursor_visible(false);
        counter_viewer.set_can_focus(false);
        counter_viewer.set_opacity(0.8);

        LineCounterItems {
            counter_tag_table,
            counter_buffer,
            counter_viewer
        }
    }
}

impl Default for LineCounterItems {
    fn default() -> Self {
        Self::new()
    }
}

pub fn update_line_numbers(text_buffer: &TextBuffer, line_numbers_buffer: &TextBuffer) {
    line_numbers_buffer.set_text("");

    let mut start = text_buffer.start_iter(); 

    let mut line_numbers_text = String::new();

    let mut line_number = 1;

    while start.forward_line() {
        line_numbers_text.push_str(&(line_number.to_string() + " \n"));
        line_number += 1;
    }
    line_numbers_text.push_str(&(line_number.to_string() + " \n"));
    line_number += 1;
    line_numbers_text.push_str(&(line_number.to_string() + " \n"));
    
    line_numbers_buffer.set_text(&line_numbers_text);
}
