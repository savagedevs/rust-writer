/*
file_menu_handler.rs - Handle and implement GTK events for the file menu.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{Application, TextBuffer, Window, AccelGroup};
use std::path::PathBuf;
use std::sync::{Arc, Mutex, MutexGuard};
use crate::structs::FileMenuItems;
use crate::save_load_backend::{save_as_buffer_to_file, load_file_into_buffer, save_buffer_to_file};

fn reset_app(active_path: &mut PathBuf, window: &Window, buffer: &TextBuffer) {
    let path: PathBuf = PathBuf::from("New File");
    let new_file: &str = path.to_str().unwrap_or("New File");
    window.set_title(format!("Rust Writer: {}", new_file).as_str());
    buffer.set_text("");
    *active_path = path;
}

fn new_init(text_buffer: &TextBuffer, file_menu: &FileMenuItems, app: &Application, active_path: &Arc<Mutex<PathBuf>>, accel_group: &AccelGroup) -> Option<()> {
    file_menu.file_new.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::N, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);
    
    let new_buffer: TextBuffer = text_buffer.clone();
    let new_window: Window = app.active_window()?;
    let new_path: Arc<Mutex<PathBuf>> = Arc::clone(active_path);
    file_menu.file_new.connect_activate(move |_| {
        let mut path: MutexGuard<'_, PathBuf> = match new_path.try_lock() {
            Ok(data) => data,
            Err(_) => return, // Return early if lock cannot be acquired
        };
        reset_app(&mut path, &new_window, &new_buffer);
        drop(path);
    });
    Some(())
}

fn save_as_init(text_buffer: &TextBuffer, file_menu: &FileMenuItems, app: &Application, active_path: &Arc<Mutex<PathBuf>>, accel_group: &AccelGroup) -> Option<()>{
    file_menu.file_save_as.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::S, 
        gtk::gdk::ModifierType::CONTROL_MASK | gtk::gdk::ModifierType::SHIFT_MASK, 
        gtk::AccelFlags::VISIBLE);

    let save_buffer: TextBuffer = text_buffer.clone();
    let save_parrent: Window = app.active_window()?;
    let save_path: Arc<Mutex<PathBuf>> = Arc::clone(active_path);
    file_menu.file_save_as.connect_activate(move |_| {
        let mut path: MutexGuard<'_, PathBuf> = match save_path.try_lock() {
            Ok(data) => data,
            Err(_) => return, // Return early if lock cannot be acquired
        };

        match save_as_buffer_to_file(&save_buffer, &save_parrent, &mut path) {
            Ok(()) => (),
            Err(_) => println!("Failed to save file")
        }
        drop(path);
    });
    Some(())
}

fn save_init(text_buffer: &TextBuffer, file_menu: &FileMenuItems, app: &Application, active_path: &Arc<Mutex<PathBuf>>, accel_group: &AccelGroup) -> Option<()>{
    file_menu.file_save.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::S, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);

    let save_buffer: TextBuffer = text_buffer.clone();
    let save_parrent: Window = app.active_window()?;
    let save_path: Arc<Mutex<PathBuf>> = Arc::clone(active_path);
    file_menu.file_save.connect_activate(move |_| {
        let mut path: MutexGuard<'_, PathBuf> = match save_path.try_lock() {
            Ok(data) => data,
            Err(_) => return, // Return early if lock cannot be acquired
        };

        match save_buffer_to_file(&save_buffer, &save_parrent, &mut path) {
            Ok(()) => (),
            Err(_) => println!("Failed to save file")
        }
        drop(path);
    });
    Some(())
}

fn load_init(text_buffer: &TextBuffer, file_menu: &FileMenuItems, app: &Application, active_path: &Arc<Mutex<PathBuf>>, accel_group: &AccelGroup) -> Option<()>{
    file_menu.file_open.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::O, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);

    let load_buffer: TextBuffer = text_buffer.clone();
    let load_parrent: Window = app.active_window()?;
    let load_path: Arc<Mutex<PathBuf>> = Arc::clone(active_path);
    file_menu.file_open.connect_activate(move |_| {
        let mut path: MutexGuard<'_, PathBuf> = match load_path.try_lock() {
            Ok(data) => data,
            Err(_) => return, // Return early if lock cannot be acquired
        };

        match load_file_into_buffer(&load_buffer, &load_parrent, &mut path) {
            Ok(()) => (),
            Err(_) => println!("Failed to load file")
        }
        drop(path);
    });
    Some(())
}

fn quit_init(file_menu: &FileMenuItems, accel_group: &AccelGroup) {
    file_menu.file_quit.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::Q, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);

    file_menu.file_quit.connect_activate(move|_| { 
        gtk::main_quit();
    });
}

pub fn file_menu_handler(file_menu: FileMenuItems, app: &Application, text_buffer: &TextBuffer, accel_group: &AccelGroup) {
    let active_path: Arc<Mutex<PathBuf>> = Arc::new(Mutex::new(PathBuf::from("New File")));
    match save_as_init(text_buffer, &file_menu, app, &active_path, accel_group) {
        Some(()) => (),
        None => println!("Could not find main window")
    }

    match save_init(text_buffer, &file_menu, app, &active_path, accel_group) {
        Some(()) => (),
        None => println!("Could not find main window")
    }

    match load_init(text_buffer, &file_menu, app, &active_path, accel_group) {
        Some(()) => (),
        None => println!("Could not find main window")
    }

    match new_init(text_buffer, &file_menu, app, &active_path, accel_group) {
        Some(()) => (),
        None => println!("Could not find main window")
    }

    quit_init(&file_menu, accel_group);
}
