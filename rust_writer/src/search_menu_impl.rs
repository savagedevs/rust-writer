/*
search_menu_impl.rs - Implementation of the SearchMenuItems struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{Menu, MenuItem};
use crate::structs::SearchMenuItems;

impl SearchMenuItems {
    pub fn new() -> Self {
        let search_submenu: MenuItem        = MenuItem::with_label("Search");
        let search_menu_holder: Menu        = Menu::new();
    
        let search_find: MenuItem           = MenuItem::with_label("Find");
        let search_find_next: MenuItem      = MenuItem::with_label("Find Next");
        let search_find_last: MenuItem      = MenuItem::with_label("Find Last");
        let search_find_replace: MenuItem   = MenuItem::with_label("Find and Replace");
    
        search_menu_holder.append(&search_find);
        search_menu_holder.append(&search_find_next);
        search_menu_holder.append(&search_find_last);
        search_menu_holder.append(&search_find_replace);
    
        search_submenu.set_submenu(Some(&search_menu_holder));

        SearchMenuItems {
            search_submenu,
            search_menu_holder,
            search_find,
            search_find_next,
            search_find_last,
            search_find_replace
        }
    }
}

impl Default for SearchMenuItems {
    fn default() -> Self {
        Self::new()
    }
}
