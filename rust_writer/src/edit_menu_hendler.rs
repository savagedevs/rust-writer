/*
edit_menu_handler.rs - Handle and implement GTK events for the edit menu.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{TextBuffer, AccelGroup};
use std::sync::{Arc, Mutex, MutexGuard};
use crate::line_counter_impl::update_line_numbers;
use crate::structs::{EditMenuItems, UndoRedoHandler};
use crate::undo_redo::{character_change_handler, undo_handler, redo_handler};

fn buffer_changed_init(text_buffer: &TextBuffer, line_counter_buffer: &TextBuffer, undo_redo_handler: &Arc<Mutex<UndoRedoHandler>>) {    
    let changed_buffer: TextBuffer = text_buffer.clone();
    let line_counter_buffer:TextBuffer = line_counter_buffer.clone();
    let undo_redo_data: Arc<Mutex<UndoRedoHandler>> = Arc::clone(undo_redo_handler);
    text_buffer.connect_changed(move |_| {
        update_line_numbers(&changed_buffer, &line_counter_buffer);
        let mut undo_redo_data: MutexGuard<'_, UndoRedoHandler> = match undo_redo_data.try_lock() {
            Ok(data) => data,
            Err(_) => return, // Return early if lock cannot be acquired
        };
        character_change_handler(&changed_buffer, &mut undo_redo_data);
        drop(undo_redo_data);
    });
}

fn undo_init(text_buffer: &TextBuffer, edit_menu: &EditMenuItems, accel_group: &AccelGroup, undo_redo_handler: &Arc<Mutex<UndoRedoHandler>>) {
    edit_menu.edit_undo.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::Z, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);

    let undo_text: TextBuffer = text_buffer.clone();
    let undo_data: Arc<Mutex<UndoRedoHandler>> = Arc::clone(undo_redo_handler);
    edit_menu.edit_undo.connect_activate(move |_| {
        let mut undo_data: MutexGuard<'_, UndoRedoHandler> = match undo_data.try_lock() {
            Ok(data) => data,
            Err(_) => return, // Return early if lock cannot be acquired
        };
        undo_handler(&undo_text, &mut undo_data);
        drop(undo_data);
    });
}

fn redo_init(text_buffer: &TextBuffer, edit_menu: &EditMenuItems, accel_group: &AccelGroup, undo_redo_handler: &Arc<Mutex<UndoRedoHandler>>) {
    edit_menu.edit_redo.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::Y, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);

    let redo_text: TextBuffer = text_buffer.clone();
    let redo_data: Arc<Mutex<UndoRedoHandler>> = Arc::clone(undo_redo_handler);
    edit_menu.edit_redo.connect_activate(move |_| {
        let mut redo_data: MutexGuard<'_, UndoRedoHandler> = match redo_data.try_lock() {
            Ok(data) => data,
            Err(_) => return, // Return early if lock cannot be acquired
        };
        redo_handler(&redo_text, &mut redo_data);
        drop(redo_data);
    });
}

fn cut_init(text_buffer: &TextBuffer, edit_menu: &EditMenuItems, accel_group: &AccelGroup,) {
    edit_menu.edit_cut.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::X, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);
    
    let edit_buffer: TextBuffer = text_buffer.clone();
    edit_menu.edit_cut.connect_activate(move |_| {
        edit_buffer.cut_clipboard(&gtk::Clipboard::get(&gtk::gdk::Atom::intern("CLIPBOARD")), true);
    });
}

fn copy_init(text_buffer: &TextBuffer, edit_menu: &EditMenuItems, accel_group: &AccelGroup,) {
    edit_menu.edit_copy.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::C, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);

    let edit_buffer: TextBuffer = text_buffer.clone();
    edit_menu.edit_copy.connect_activate(move |_| {
        edit_buffer.copy_clipboard(&gtk::Clipboard::get(&gtk::gdk::Atom::intern("CLIPBOARD")));
    });
}

fn paste_text_into_buffer(edit_buffer: &TextBuffer) -> Option<()> {
    let clipboard: gtk::Clipboard = gtk::Clipboard::get(&gtk::gdk::Atom::intern("CLIPBOARD"));
    if let Some((mut start, mut end)) = edit_buffer.selection_bounds() {
        let text: gtk::glib::GString = clipboard.wait_for_text()?;
        if start != end {
            edit_buffer.delete(&mut start, &mut end);
        }
        edit_buffer.insert(&mut start, &text);   
    } else {
        let mut iter: gtk::TextIter= edit_buffer.iter_at_mark(&edit_buffer.get_insert()?);
        let clipboard: gtk::Clipboard = gtk::Clipboard::get(&gtk::gdk::Atom::intern("CLIPBOARD"));
        let text: gtk::glib::GString = clipboard.wait_for_text()?;
        edit_buffer.insert(&mut iter, &text);
    }

    Some(())
}

fn paste_init(text_buffer: &TextBuffer, edit_menu: &EditMenuItems, accel_group: &AccelGroup,) {
    edit_menu.edit_paste.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::V, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);

    let edit_buffer: TextBuffer = text_buffer.clone();
    edit_menu.edit_paste.connect_activate(move |_| {
        match paste_text_into_buffer(&edit_buffer) {
            Some(()) => (),
            None => println!("Could not paste")
        }
    });
}

fn preferences_init(edit_menu: &EditMenuItems, accel_group: &AccelGroup,) {
    edit_menu.edit_preferences.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::P, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);
    
    edit_menu.edit_preferences.connect_activate(move |_| {
        println!("This is unimplemented");

        //TODO: implement it
    });
}

pub fn edit_menu_handler(text_buffer: &TextBuffer, line_counter_buffer: &TextBuffer, edit_menu: &EditMenuItems, accel_group: &AccelGroup, undo_redo_handler: &Arc<Mutex<UndoRedoHandler>>) {
    buffer_changed_init(text_buffer, line_counter_buffer, undo_redo_handler);
    undo_init(text_buffer, edit_menu, accel_group, undo_redo_handler);
    redo_init(text_buffer, edit_menu, accel_group, undo_redo_handler);
    cut_init(text_buffer, edit_menu, accel_group);
    copy_init(text_buffer, edit_menu, accel_group);
    paste_init(text_buffer, edit_menu, accel_group);
    preferences_init(edit_menu, accel_group);
}
