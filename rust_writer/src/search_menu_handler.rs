/*
search_menu_handler.rs - Handle and implement GTK events for the search menu.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::AccelGroup;
use crate::structs::SearchMenuItems;

fn find_init(search_menu: &SearchMenuItems, accel_group: &AccelGroup) {
    search_menu.search_find.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::F, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);
    
    search_menu.search_find.connect_activate(move |_| {
        println!("This is unimplemented");

        //TODO: implement it
    });
}

fn find_next_init(search_menu: &SearchMenuItems, accel_group: &AccelGroup) {
    search_menu.search_find_next.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::G, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);
    
    search_menu.search_find_next.connect_activate(move |_| {
        println!("This is unimplemented");

        //TODO: implement it
    });
}

fn find_last_init(search_menu: &SearchMenuItems, accel_group: &AccelGroup) {
    search_menu.search_find_last.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::G, 
        gtk::gdk::ModifierType::CONTROL_MASK | gtk::gdk::ModifierType::SHIFT_MASK, 
        gtk::AccelFlags::VISIBLE);
    
    search_menu.search_find_last.connect_activate(move |_| {
        println!("This is unimplemented");

        //TODO: implement it
    });
}

fn find_replace_init(search_menu: &SearchMenuItems, accel_group: &AccelGroup) {
    search_menu.search_find_replace.add_accelerator(
        "activate", 
        accel_group, 
        *gtk::gdk::keys::constants::R, 
        gtk::gdk::ModifierType::CONTROL_MASK, 
        gtk::AccelFlags::VISIBLE);
    
    search_menu.search_find_replace.connect_activate(move |_| {
        println!("This is unimplemented");

        //TODO: implement it
    });
}

pub fn search_menu_handler(search_menu: &SearchMenuItems, accel_group: &AccelGroup) {
    find_init(search_menu, accel_group);
    find_next_init(search_menu, accel_group);
    find_last_init(search_menu, accel_group);
    find_replace_init(search_menu, accel_group);
}
