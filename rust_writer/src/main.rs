/*
Rust Writer - A Rust/GTK 3.0 based memory safe simple plain text editor

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
pub mod structs;

pub mod header_bar_impl;
pub mod search_menu_impl;
pub mod edit_menu_impl;
pub mod file_menu_impl;
pub mod window_content_impl;
pub mod active_text_impl;
pub mod unre_handler_impl;
pub mod line_counter_impl;

pub mod event_handler;
pub mod file_menu_handler;
pub mod edit_menu_hendler;
pub mod search_menu_handler;

pub mod save_load_backend;
pub mod undo_redo;
pub mod user_interface;

use gtk::prelude::*;
use gtk::Application;
use crate::event_handler::run_app;

fn main() {
    gtk::init().unwrap();

    let app = Application::builder()
        .application_id("com.savagedevs.rust_writer")
        .build();

    let cloned_app = app.clone();
    app.connect_activate(move |_| {
        run_app(&cloned_app);
    });
    app.run();
}
