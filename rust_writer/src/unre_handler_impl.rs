/*
unre_buffer_impl.rs - Implementation of the UndoRedoBuffer struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use crate::structs::UndoRedoHandler;

impl UndoRedoHandler {
    pub fn new() -> Self {
        UndoRedoHandler {
            history: Vec::new(),
            undo_stack: Vec::new(),
        }
    }

    pub fn push(&mut self, item: String) {
        self.history.push(item);
        self.undo_stack.clear();
    }

    pub fn undo(&mut self) -> Option<String> {
        if let Some(item) = self.history.pop() {
            self.undo_stack.push(item.clone());
            self.history.last().cloned()
        } else {
            None
        }
    }

    pub fn redo(&mut self) -> Option<String> {
        if let Some(item) = self.undo_stack.pop() {
            self.history.push(item.clone());
            self.history.last().cloned()
        } else {
            None
        }
    }
}

impl Default for UndoRedoHandler {
    fn default() -> Self {
        Self::new()
    }
}
