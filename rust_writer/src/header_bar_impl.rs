/*
search_menu_impl.rs - Implementation of the SearchMenuItems struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{HeaderBar, MenuBar};
use crate::structs::{EditMenuItems, FileMenuItems, HeaderBarItems, SearchMenuItems};

impl HeaderBarItems {
    pub fn new() -> Self {
        let header_bar: HeaderBar                  = HeaderBar::new();
        let menu_bar: MenuBar                      = MenuBar::new();

        let file_menu_items: FileMenuItems         = FileMenuItems::new();
        let edit_menu_items: EditMenuItems         = EditMenuItems::new();
        let search_menu_items: SearchMenuItems     = SearchMenuItems::new();
    
        menu_bar.append(&file_menu_items.file_submenu);
        menu_bar.append(&edit_menu_items.edit_submenu);
        menu_bar.append(&search_menu_items.search_submenu);

        header_bar.set_show_close_button(true);
        header_bar.pack_start(&menu_bar);

        HeaderBarItems {
            header_bar,
            file_menu_items,
            edit_menu_items,
            search_menu_items
        }
    }
}

impl Default for HeaderBarItems {
    fn default() -> Self {
        Self::new()
    }
}
