use std::path::PathBuf;

/*
structs.rs - Top level definition of application wide structs

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::{Menu, HeaderBar, MenuItem, TextBuffer, TextTagTable, TextView, ScrolledWindow};

#[derive(Debug, Clone, PartialEq)]
pub struct FileMenuItems {
    pub file_submenu:           MenuItem,
    pub file_menu_holder:       Menu,

    pub file_new:               MenuItem,
    pub file_open:              MenuItem,
    pub file_save:              MenuItem,
    pub file_save_as:           MenuItem,
    pub file_quit:              MenuItem,
}

#[derive(Debug, Clone, PartialEq)]
pub struct EditMenuItems {
    pub edit_submenu:           MenuItem,
    pub edit_menu_holder:       Menu,

    pub edit_undo:              MenuItem,
    pub edit_redo:              MenuItem,
    pub edit_cut:               MenuItem,
    pub edit_copy:              MenuItem,
    pub edit_paste:             MenuItem,
    pub edit_preferences:       MenuItem,
}

#[derive(Debug, Clone, PartialEq)]
pub struct SearchMenuItems {
    pub search_submenu:         MenuItem,
    pub search_menu_holder:     Menu,

    pub search_find:            MenuItem,
    pub search_find_next:       MenuItem,
    pub search_find_last:       MenuItem,
    pub search_find_replace:    MenuItem,
}

#[derive(Debug, Clone, PartialEq)]
pub struct HeaderBarItems {
    pub header_bar:             HeaderBar,
    pub file_menu_items:        FileMenuItems,
    pub edit_menu_items:        EditMenuItems,
    pub search_menu_items:      SearchMenuItems,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ActiveTextItems {
    pub text_tag_table:         TextTagTable,
    pub text_buffer:            TextBuffer,
    pub text_viewer:            TextView,
}

#[derive(Debug, Clone, PartialEq)]
pub struct LineCounterItems {
    pub counter_tag_table:      TextTagTable,
    pub counter_buffer:         TextBuffer,
    pub counter_viewer:         TextView,
}

#[derive(Debug, Clone, PartialEq)]
pub struct WindowContentItems {
    pub scrolled_window:        ScrolledWindow,
    pub active_text_items:      ActiveTextItems,
    pub line_counter_items:     LineCounterItems,
    pub active_file:            PathBuf,
}

#[derive(Debug, Clone, PartialEq)]
pub struct UndoRedoHandler {
    pub history:                Vec<String>,
    pub undo_stack:             Vec<String>,
}
