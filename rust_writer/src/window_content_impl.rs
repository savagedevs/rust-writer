use std::path::PathBuf;

/*
window_content_impl.rs - Implementation of the WindowContentItems struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{Adjustment, Box, Orientation, PolicyType, ScrolledWindow};
use crate::structs::{WindowContentItems, ActiveTextItems, LineCounterItems};
use crate::line_counter_impl::update_line_numbers;

impl WindowContentItems {
    pub fn new() -> Self {
        let window_box = Box::new(Orientation::Horizontal, 1);

        let active_text_items = ActiveTextItems::new();
        let line_counter_items = LineCounterItems::new();
        window_box.add(&line_counter_items.counter_viewer);
        window_box.add(&active_text_items.text_viewer);
    
        let scrolled_window = ScrolledWindow::new(None::<&Adjustment>, None::<&Adjustment>);
        scrolled_window.add(&window_box);
        scrolled_window.set_policy(PolicyType::Automatic, PolicyType::Automatic);

        update_line_numbers(&active_text_items.text_buffer, &line_counter_items.counter_buffer);
        let active_file: PathBuf = PathBuf::from("New File");

        WindowContentItems {
            scrolled_window,
            active_text_items,
            line_counter_items,
            active_file
        }
    }
}

impl Default for WindowContentItems {
    fn default() -> Self {
        Self::new()
    }
}
