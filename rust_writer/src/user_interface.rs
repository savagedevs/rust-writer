/*
user_interface.rs - Define the top level of the user interface

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{
    gdk_pixbuf::{Pixbuf, PixbufLoader}, 
    Application, ApplicationWindow, AccelGroup
};
use std::error::Error;
use crate::structs::{HeaderBarItems, WindowContentItems};

fn set_icon() -> Result<Pixbuf, Box<dyn Error>> {
    let icon_from_png = include_bytes!("icon.png");

    let pixel_buffer_loader = PixbufLoader::new();
    pixel_buffer_loader.write(icon_from_png)?;
    pixel_buffer_loader.close()?;

    let pixbuf = pixel_buffer_loader.pixbuf().ok_or("Failed to get GString")?;
    Ok(pixbuf)
}

pub fn build_ui(app: &Application, window_content: &WindowContentItems, header_bar: &HeaderBarItems, accel_group: &AccelGroup) {
    let window: ApplicationWindow = ApplicationWindow::builder()
        .application(app)
        .child(&window_content.scrolled_window)
        .build();

    if let Ok(icon) = set_icon() {
        window.set_icon(Some(&icon));
    } else {
        println!("Could not set app icon")
    }

    window.add_accel_group(accel_group);

    let new_file: &str = window_content.active_file.to_str().unwrap_or("New File");

    window.set_titlebar(Some(&header_bar.header_bar));
    window.set_title(format!("Rust Writer: {}", new_file).as_str());
    window.show_all();
}
