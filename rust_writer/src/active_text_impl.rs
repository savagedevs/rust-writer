/*
active_text_impl.rs - Implementation of the ActiveTextItems struct

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 Daniel McLarty

Maintainer(s): Daniel McLarty <daniel@savagedves.com>
*/
use gtk::prelude::*;
use gtk::{TextBuffer, TextTagTable, TextView, WrapMode};
use crate::structs::ActiveTextItems;

impl ActiveTextItems {
    pub fn new() -> Self {
        let text_tag_table = TextTagTable::new();
        let text_buffer = TextBuffer::new(Some(&text_tag_table));
        let text_viewer = TextView::with_buffer(&text_buffer);
    
        text_viewer.set_wrap_mode(WrapMode::None);
        text_viewer.set_hexpand(true);
        text_viewer.set_vexpand(true);

        ActiveTextItems {
            text_tag_table,
            text_buffer,
            text_viewer
        }
    }
}

impl Default for ActiveTextItems {
    fn default() -> Self {
        Self::new()
    }
}
