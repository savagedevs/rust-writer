# Contributing to Rust Writer

Thank you for considering contributing to Rust Writer! Before submitting your contributions, please take a moment to review the guidelines outlined below.

## Project Overview

Rust Writer is a memory safe notepad like plain text editor using the GTK 3.0 toolkit.

## Development Environment

Contributors are encouraged to utilize the provided shell.nix file for consistent development environments.

## Code Style Guidelines

- Variable and function names should follow snake_case convention.
- Use descriptive names to enhance code readability; avoid one-letter variables or poorly named identifiers.
- Code should be self-explanatory; avoid unnecessary comments explaining what the code does.

## Code Formatting

- Code formatting should adhere to the Rust style guide. It is also recommended to sue rustfmt and clippy.
- All source files should use the following header with the right information filled in:
```rust
/*
name - one line

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

(C) 2024 First Last

Maintainer(s): First Last <your@email.tld>
*/
```

## Testing Guidelines

- The compiler does most of the testing for safety, just make sure your code works.

## Documentation

- It is self explanitory

## Contributing Process and Review

1. Fork the repository on GitLab.
2. Push your changes to the dev branch in your fork.
3. Submit a merge request to the dev branch of the main repository.
4. Contributions will be reviewed functionality on the maintainers' machines.
5. Approved changes will be merged into the dev branch for further testing.

## Licensing

All contributions to Rust Writer are subject to the terms of the Mozilla Public License (MPL). Please review the LICENSE.md file for details.

## Guidelines

Contributors are expected to adhere to the following guidelines:

- Don't be a jerk.
- Write high-quality code; no excuses.
- Accept constructive criticism from maintainers regarding code quality.
- Talk only of the code; avoid engaging in politics or identity-related issues.

By contributing to Rust Writer, you agree to abide by these guidelines and contribute positively to the project's growth and success.

Thank you for your interest in contributing to Rust Writer!
