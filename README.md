# Rust Writer

Rust Writer is a memory safe notepad like plain text editor using the GTK 3.0 toolkit.

## Installation

Currently, the only way to install Rust Writer is by compiling from source using Cargo, Rust's package manager. Once compiled, the output binaries need to be moved to the appropriate locations in the PATH. 

## Usage

It is a text editor, not much else to say, it loads files, lets you type, etc.

## Contributing

Please refer to the [CONTRIBUTING.md](CONTRIBUTING.md) file for guidelines on contributing to Rust Writer.

## License

This project is licensed under the Mozilla Public License 2.0. See the [LICENSE](LICENSE) file for details.

## Additional Information

- For questions or discussions, please open an issue on the GitLab repository.
- Contributions are welcome and appreciated! If you find a bug or have a feature request, feel free to submit a pull request.
- Rust Writer is a work in progress. Your feedback and contributions help improve the project and make it more robust.
