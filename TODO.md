# Rust Writer TODO List

## Backlog

### Core Functionality
- Implement find functionality.
- Implement replace functionality.

### User Interface
- Implement the menu bar.

## Done

### Core Functionality
- Implement line numbering feature.
- Add support for undo and redo operations.
- Implement file full saving functionality.
- Implement file full loading functionality.
- Add ability to select text.
- Implement basic text insertion functionality.
- Implement text deletion capability (single character and selection).
- Add support for UTF-8 encoding.
- Implement copy and paste functionality.


### User Interface
- Design main window layout.
- Implement text area widget.
- Create a basic empty window.
- Design the menu bar.
- Implement keyboard shortcuts for common actions.

## Icebox

### Advanced Features
- Implement syntax highlighting for programming languages.
- Add support for customizable keybindings.
- Implement basic plugin system for extending functionality.

## Continuous

### Maintenance
- Regularly update dependencies.
- Address reported bugs and issues.
- Refactor codebase for improved readability and maintainability.
- Document code and provide usage instructions.
